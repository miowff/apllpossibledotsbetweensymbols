﻿
namespace DotsBetweenSymbolsNew
{
    public sealed class StringHandler
    {
        private readonly string input;
        private List<string> result = new List<string>();
        private readonly int maxDots;
        public StringHandler(string input)
        {
            this.input = input;
            this.maxDots = input.Length - 1;
        }
        public string[] GetResult()
        {
            return result.ToArray();
        }
        public void CreateResult()
        {
            for (int i = 0; i <= maxDots; i++)
            {
                GetAllCombos(i);
            }
        }
        void GetAllCombos(int k)
        {
            int[] indexesCombos = new int[k + 2];
            for (int i = 0; i < k; i++)
            {
                indexesCombos[i] = i;
            }
            indexesCombos[k] = maxDots;
            indexesCombos[k + 1] = 0;
            while (true)
            {
                SetDots(IndexesForInsert(indexesCombos, 0, k), input);
                int j = 0;
                while (indexesCombos[j] + 1 == indexesCombos[j + 1])
                {
                    indexesCombos[j] = j;
                    j++;
                }
                if (j < k)
                {
                    indexesCombos[j]++;
                }
                else
                {
                    break;
                }
            }
        }
        List<int> IndexesForInsert(int[] array, int s, int e)
        {
            var indexesForInsert = new List<int>();
            for (int i = s; i < e; i++)
            {
                indexesForInsert.Add(array[i]);
            }
            return indexesForInsert;
        }

        void SetDots(List<int> indexes, string inp)
        {
            int dotsCounter = 1;
            for (int i = 0; i < indexes.Count; i++)
            {
                inp = inp.Insert(indexes[i] + dotsCounter, ".");
                dotsCounter++;
            }
            this.result.Add(inp);
        }
    }
}
