﻿using DotsBetweenSymbolsNew;

Console.WriteLine("Write your string");
var input = Console.ReadLine();

while (string.IsNullOrWhiteSpace(input))
{
    Console.WriteLine("Wrong input\n try again");
    input = Console.ReadLine();
}

var stringHandler = new StringHandler(input);
stringHandler.CreateResult();

var result = stringHandler.GetResult();
foreach(var res in result)
{
    Console.WriteLine(res);
}
